import json
import websocket
import threading
import requests
import pandas as pd

# API information, API key & secret should be secured
API_ENDPOINT = "wss://testnet.thalex.com/ws/api/v2"
API_KEY = "Your API Key Here"
API_SECRET = "Your API Secret Here"
INSTRUMENT_NAME = "ETH-PERPETUAL"
GROUPING = "none"
NLEVELS = "10"
DELAY = "1000ms"

# Placeholder for the order book DataFrame
order_book_df = pd.DataFrame(columns=['price', 'quantity'])

# Function to authenticate the WebSocket connection
def authenticate(ws):
    # Normally, here you'd construct a sign request with your API_SECRET
    auth_request = {
        "method": "public/auth",
        "params": {
            "token": API_KEY
        }
    }
    ws.send(json.dumps(auth_request))

# Function to send a message to WebSocket server
def send_message(ws, message):
    ws.send(json.dumps(message))

def fetch_index(underlying):
    endpoint = 'public/index'
    params = {'underlying': underlying}
    url = f'https://testnet.thalex.com/api/v2/{endpoint}'
    resp = requests.get(url, params=params)
    if resp.status_code == 200:
        return resp.json()['result']['price']
    else:
        print("Failed to fetch index price")
        return None

# MQS calculation logic
def calculate_mqs(df, index_price, bps=2, exp=0.1):
    typ_dist = index_price * bps * 10**-4
    best_bid = df['price'].max()  # Assuming descending order of bids
    df['D'] = abs(df['price'] - best_bid)
    df['D_norm'] = df['D'] / typ_dist
    df['P_score'] = df['D_norm'].apply(lambda norm_dist: exp ** norm_dist)
    df['TOBE'] = df['P_score'] * df['quantity']
    df['MQS'] = df['TOBE'] / df['TOBE'].sum()
    return df

# The function `place_order` would be implemented to send an order placement message to the exchange
def place_order(ws, price, quantity, side):
    order_request = {
        "method": "private/buy" if side == 'buy' else "private/sell",
        "params": {
            "instrument_name": INSTRUMENT_NAME,
            "price": price,
            "quantity": quantity,
            "type": "limit"
        }
    }
    send_message(ws, order_request)
    print(f"Order placed: {order_request}")

# The function `cancel_order` would be implemented to send an order cancellation message to the exchange
def cancel_order(ws, order_id):
    cancel_request = {
        "method": "private/cancel",
        "params": {
            "order_id": order_id
        }
    }
    send_message(ws, cancel_request)
    print(f"Order cancellation requested for order id: {order_id}")

# WebSocket callbacks
def on_open(ws):
    print("Opened connection")
    authenticate(ws)
    subscription_request = {
        "method": "private/subscribe",
        "params": {
            "channels": [f"book.{INSTRUMENT_NAME}.{GROUPING}.{NLEVELS}.{DELAY}"]
        }
    }
    send_message(ws, subscription_request)

def on_message(ws, message):
    print("Received message")
    data = json.loads(message)
    if 'book.' in data['channel_name']:
        handle_order_book_update(data['notification'])

def handle_order_book_update(ws, notification, my_last_order_id=None):
    # Your existing order book parsing and MQS calculation logic...
    
    # Calculate the new order price and quantity
    new_order_price = best_bid_price - 1  # Your new order price is 1 USD below the best bid
    new_order_quantity = 0.1  # The quantity for the new order is always 0.1

    # Check if your hypothetical order would have a better MQS
    my_order_index = order_book_df[order_book_df['P'] < new_order_price].index.max()
    if my_order_index is not None and order_book_df.loc[my_order_index, 'MQS'] < order_book_df[order_book_df['P'] > new_order_price]['MQS'].max():
        print("Placing a new order at price:", new_order_price)
        place_order(ws, new_order_price, new_order_quantity, 'buy')
        if my_last_order_id is not None:
            print("Canceling the old order")
            cancel_order(ws, my_last_order_id)
    else:
        print("No need to place a new order, the current MQS is competitive.")

    # Update the last order ID with the new one, after placing an order you should receive an order ID in response
    # my_last_order_id = response_order_id  # Save the new order ID

def on_message(ws, message):
    print("Received message")
    data = json.loads(message)
    # Check if we have an order book update
    if 'book.' in data['channel_name']:
        handle_order_book_update(ws, data['notification'])

def on_error(ws, error):
    print(f"Error: {error}")

def on_close(ws):
    print("Closed connection")

# Establish a connection to the WebSocket API
def run_bot():
    ws_app = websocket.WebSocketApp(API_ENDPOINT,
                                    on_open=on_open,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close)

    ws_app.run_forever()

if __name__ == "__main__":
    # Run the bot
    bot_thread = threading.Thread(target=run_bot)
    bot_thread.start()

"""

import requests

def fetch_order_book(instrument_name):
    url = 'https://testnet.thalex.com/api/v2/public/book'
    params = {'instrument_name': instrument_name}
    resp = requests.get(url, params=params)
    if resp.status_code == 200:
        order_book_data = resp.json()['result']
        return process_order_book(order_book_data)
    else:
        print("Failed to fetch order book data")
        return None
        

def calculate_mqs(order_book, depth_cutoff, typical_distance, discount_exponent):
    # Process bids and asks separately, here's an example for bids
    bids = order_book['bids']
    mqs_scores = []

    # Assuming bids are sorted with the best bid first
    best_bid = bids[0][0] if bids else 0
    cumulative_size = 0

    for price, size, _ in bids:
        # Apply Depth Cutoff
        if cumulative_size < depth_cutoff:
            remaining_depth = depth_cutoff - cumulative_size
            size = min(size, remaining_depth)
            cumulative_size += size

            # Calculate Distance Discounting
            price_distance = abs(best_bid - price)
            normalized_distance = price_distance / typical_distance
            price_score = discount_exponent ** normalized_distance

            # Calculate TOBE and MQS
            tobe = size * price_score
            mqs_scores.append((price, tobe))
        else:
            break

    # Normalize MQS scores
    total_tobe = sum(tobe for _, tobe in mqs_scores)
    mqs_scores = [(price, tobe / total_tobe) for price, tobe in mqs_scores]

    return mqs_scores


def handle_order_book_update(notification, depth_cutoff, typical_distance, discount_exponent):
    order_book = notification['result']
    mqs_scores = calculate_mqs(order_book, depth_cutoff, typical_distance, discount_exponent)
    # Additional logic to make trading decisions based on MQS scores...

    
"""